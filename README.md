# Exercise

A simple application which displays users fetched from jsonplaceholder.typicode.com in an
alphabetical order. By clicking a user, you will be shown additional details about the user and
all posts made by said user.

## Installation instructions

1. Clone repository
2. Install all required build tools via SDK manager
3. Sync gradle
4. Run application
