package com.toni.exercise.managers;

// Events dispatched to indicate a finished API call
public interface EventListener<T> {
    String USERS_FETCHED = "USERS_FETCHED";
    String USER_IMAGE_FETCHED = "USER_IMAGE_FETCHED";
    String USER_POSTS_FETCHED = "USER_POSTS_FETCHED";

    void onEventReceived(String event, T data);
}
