package com.toni.exercise.managers;

import java.util.ArrayList;
import java.util.List;

// Register and unregister listeners and dispatch events to all registered listeners
public class EventManagerSingleton {
    private static final EventManagerSingleton ourInstance = new EventManagerSingleton();

    private List<EventListener> callbacks = new ArrayList<>();

    public static EventManagerSingleton getInstance() {
        return ourInstance;
    }

    private EventManagerSingleton() { }

    public void addListener(EventListener listener) {
        if (!callbacks.contains(listener)) {
            callbacks.add(listener);
        }
    }

    public void removeListener(EventListener listener) {
        if (callbacks.contains(listener)) {
            callbacks.remove(listener);
        }
    }

    public void usersFetched(ArrayList users) {
        for (EventListener callback : callbacks) {
            callback.onEventReceived(EventListener.USERS_FETCHED, users);
        }
    }

    public void userPostsFetch(ArrayList posts) {
        for (EventListener callback : callbacks) {
            callback.onEventReceived(EventListener.USER_POSTS_FETCHED, posts);
        }
    }
}
