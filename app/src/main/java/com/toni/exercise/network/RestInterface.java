package com.toni.exercise.network;

public interface RestInterface {
    String BASE_URL = "https://jsonplaceholder.typicode.com";
    String ENDPOINT_USERS = "/users";
    String ENDPOINT_PHOTOS = "/photos";
    String ENDPOINT_POSTS = "/posts";
}
