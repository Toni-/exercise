package com.toni.exercise.network;

import android.os.AsyncTask;
import android.util.Log;

import com.toni.exercise.managers.EventManagerSingleton;
import com.toni.exercise.models.User;
import com.toni.exercise.utils.PostUtils;
import com.toni.exercise.utils.UserComparator;
import com.toni.exercise.utils.UserUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


// TODO: Missing error handling for now
public class UserRequest implements RestInterface {
    final public static String TAG = "UserRequest";

    public static void getUsers() {
        final OkHttpClient client = new OkHttpClient();

        final Request request = new Request.Builder()
                .url(BASE_URL+ENDPOINT_USERS)
                .method("GET", null)
                .build();

        AsyncTask task = new AsyncTask() {
            @Override
            protected String doInBackground(Object[] objects) {
                String jsonResponse = "";
                try {
                    Response response = client.newCall(request).execute();

                    Log.d(TAG, "Response success: " + response.isSuccessful());

                    jsonResponse = response.body().string();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return jsonResponse;
            }

            @Override
            protected void onPostExecute(Object o) {
                // Parse data into an arraylist
                ArrayList userList = UserUtils.getUserList(o.toString());

                // Sort alphabetically
                Collections.sort(userList, new UserComparator());

                // Dispatch an event informing the activity that the data has been fetched
                EventManagerSingleton.getInstance().usersFetched(userList);
            }
        };

        task.execute();
    }

    public static void getUserPosts(int userId) {
        final OkHttpClient client = new OkHttpClient();

        String url = BASE_URL+ENDPOINT_POSTS+"?userId="+userId;

        final Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();

        AsyncTask task = new AsyncTask() {
            @Override
            protected String doInBackground(Object[] objects) {
                String jsonResponse = "";
                try {
                    Response response = client.newCall(request).execute();

                    Log.d(TAG, "Response success: " + response.isSuccessful());

                    jsonResponse = response.body().string();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return jsonResponse;
            }

            @Override
            protected void onPostExecute(Object o) {
                EventManagerSingleton.getInstance().userPostsFetch(PostUtils.getPostList(o.toString()));
            }
        };

        task.execute();
    }
}
