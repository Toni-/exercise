package com.toni.exercise.utils;

import com.google.gson.Gson;
import com.toni.exercise.models.Post;

import java.util.ArrayList;
import java.util.Arrays;

public class PostUtils {

    // Returns a parsed list of users, return an empty list if something goes wrong
    public static ArrayList<Post> getPostList(String json) {
        try {
            return new ArrayList<Post>(Arrays.asList(parsePostData(json)));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    // Parse given json string input
    private static Post[] parsePostData(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Post[].class);
    }
}
