package com.toni.exercise.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.toni.exercise.models.User;

import java.util.ArrayList;
import java.util.Arrays;

public class UserUtils {

    // Returns a parsed list of users, return an empty list if something goes wrong
    public static ArrayList<User> getUserList(String json) {
        try {
            return new ArrayList<User>(Arrays.asList(parseUserData(json)));
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    // Parse given json string input
    private static User[] parseUserData(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, User[].class);
    }
}
