package com.toni.exercise.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toni.exercise.R;
import com.toni.exercise.managers.EventListener;
import com.toni.exercise.managers.EventManagerSingleton;
import com.toni.exercise.models.User;
import com.toni.exercise.network.UserRequest;
import com.toni.exercise.views.adapters.UserPostsRecyclerViewAdapter;

import java.util.ArrayList;

public class UserDetailsActivity extends AppCompatActivity implements EventListener {

    private TextView textViewName;
    private TextView textViewEmail;
    private TextView textViewPhone;
    private TextView textViewNameShorthand;
    private RecyclerView recyclerViewPosts;
    private LinearLayout linearLayoutPosts;
    private LinearLayout linearLayoutSpinner;
    private UserPostsRecyclerViewAdapter postsRecyclerViewAdapter;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        EventManagerSingleton.getInstance().addListener(this);

        user = getIntent().getParcelableExtra(getString(R.string.intent_extra_user));

        textViewName = findViewById(R.id.user_details_name_text);
        textViewEmail = findViewById(R.id.user_details_email_text);
        textViewPhone = findViewById(R.id.user_details_phone_text);
        textViewNameShorthand = findViewById(R.id.user_details_text_name_shorthand);
        recyclerViewPosts = findViewById(R.id.user_details_recycler_view);
        linearLayoutPosts = findViewById(R.id.user_details_recycler_view_container);
        linearLayoutSpinner = findViewById(R.id.user_details_spinner);

        // Fetch posts to be displayed just for the heck of it
        UserRequest.getUserPosts(user.getId());

        initializeViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventManagerSingleton.getInstance().removeListener(this);
    }

    @Override
    public void onEventReceived(String event, Object data) {

        // Posts fetched, hide spinner, show post list
        if (event == USER_POSTS_FETCHED) {
            postsRecyclerViewAdapter = new UserPostsRecyclerViewAdapter(this, (ArrayList) data);
            recyclerViewPosts.setAdapter(postsRecyclerViewAdapter);
            recyclerViewPosts.setLayoutManager(new LinearLayoutManager(this));

            linearLayoutSpinner.setVisibility(View.GONE);
            linearLayoutPosts.setVisibility(View.VISIBLE);
        }
    }

    private void initializeViews() {
        textViewName.setText(user.getName());
        textViewEmail.setText(user.getEmail());
        textViewPhone.setText(user.getPhone());
        textViewNameShorthand.setText(String.valueOf(user.getName().charAt(0)));
    }
}
