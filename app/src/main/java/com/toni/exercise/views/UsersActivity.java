package com.toni.exercise.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.toni.exercise.R;
import com.toni.exercise.managers.EventListener;
import com.toni.exercise.managers.EventManagerSingleton;
import com.toni.exercise.network.UserRequest;
import com.toni.exercise.views.adapters.UsersRecyclerViewAdapter;

import java.util.ArrayList;


public class UsersActivity extends AppCompatActivity implements EventListener {
    public static final String TAG = "UsersActivity";

    private RecyclerView recyclerView;
    private UsersRecyclerViewAdapter recyclerViewAdapter;
    private LinearLayout spinnerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        recyclerView = findViewById(R.id.users_recycler_view);
        spinnerLayout = findViewById(R.id.users_spinner);

        EventManagerSingleton.getInstance().addListener(this);

        UserRequest.getUsers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventManagerSingleton.getInstance().removeListener(this);
    }

    @Override
    public void onEventReceived(String event, Object data) {

        // Users fetched, hide spinner, show user list
        if (event == USERS_FETCHED) {
            recyclerViewAdapter = new UsersRecyclerViewAdapter(this, (ArrayList) data);
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            spinnerLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
