package com.toni.exercise.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.toni.exercise.R;
import com.toni.exercise.models.Post;

import java.util.ArrayList;

public class UserPostsRecyclerViewAdapter extends RecyclerView.Adapter<UserPostsRecyclerViewAdapter.ViewHolder> {
    private ArrayList<Post> posts;
    private Context context;

    public UserPostsRecyclerViewAdapter(Context context, ArrayList posts) {
        this.context = context;
        this.posts = posts;
    }

    @NonNull
    @Override
    public UserPostsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_details_post_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserPostsRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.textViewTitle.setText(posts.get(position).getTitle());
        holder.textViewBody.setText(posts.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewBody;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.user_details_list_item_title_text);
            textViewBody = itemView.findViewById(R.id.user_details_list_item_body_text);
        }
    }
}
