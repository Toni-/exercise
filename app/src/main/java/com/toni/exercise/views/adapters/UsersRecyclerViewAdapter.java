package com.toni.exercise.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toni.exercise.R;
import com.toni.exercise.models.User;
import com.toni.exercise.views.UserDetailsActivity;

import java.util.ArrayList;


public class UsersRecyclerViewAdapter extends RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder> {
    private final static String TAG = "UsersRecViewAdapter";
    private ArrayList<User> users;
    private Context context;

    public UsersRecyclerViewAdapter(Context context, ArrayList<User> users) {
        this.users = users;
        this.context = context;
    }

    @NonNull
    @Override
    public UsersRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int i = holder.getAdapterPosition();

        holder.textViewName.setText(users.get(i).getName());
        holder.textViewEmail.setText(users.get(i).getEmail());

        // User row was supposed to have a user image on the left hand side but then I realized there are
        // no images for users so a letter indicating each user is shown instead.
        holder.textViewNameShorthand.setText(String.valueOf(users.get(i).getName().charAt(0)));

        holder.linearLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UserDetailsActivity.class);
                intent.putExtra(context.getString(R.string.intent_extra_user), users.get(i));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewNameShorthand;
        TextView textViewName;
        TextView textViewEmail;
        LinearLayout linearLayoutRoot;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewNameShorthand = itemView.findViewById(R.id.users_list_item_text_name_shorthand);
            textViewName = itemView.findViewById(R.id.users_list_item_text_name);
            textViewEmail = itemView.findViewById(R.id.users_list_item_text_email);
            linearLayoutRoot = itemView.findViewById(R.id.users_list_item_root);
        }
    }
}
